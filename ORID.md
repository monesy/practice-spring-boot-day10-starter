### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	上午进行了Code Review。今天学习了SpringBoot mapper，然后学习了使用内存数据库flyway连接Spring Boot。下午进行了Retro，并进行了Container的汇报，我负责的是定义和原理部分。

### R (Reflective): Please use one word to express your feelings about today's class.

​	非常有意义

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	今天学习的内容不全是编程知识方面的，还有关于工作后的演说汇报和Retro方面的学习，这让我感觉很新奇，并且很有意义，这让我对团队合作和团队之间协调工作有了更多的经验，也增强了团队的凝聚力。

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	要多多锻炼自己的语言表达能力和演说能力。
